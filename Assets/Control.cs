﻿using UnityEngine;
using System.Collections;

public class Control : MonoBehaviour {
    private float maxSpeed;
    public static float size;
    public static bool YouNowFood;

    public float minimum = 0f;
    public float maximum = 1f;
    public float duration = 1f;
    public static float startTime;

    public SpriteRenderer sprite;
    // Use this for initialization
    void Start () {
        size = 1f;
        YouNowFood = false;
	}
	
	// Update is called once per frame
	void Update () {

        if (!YouNowFood)
        {
            maxSpeed = 2 / size;
            float moveX = Input.GetAxis("Horizontal");
            float moveY = Input.GetAxis("Vertical");

            GetComponent<Rigidbody2D>().velocity = new Vector2(moveX * maxSpeed, moveY * maxSpeed);

            transform.localScale = new Vector3(size, size, 0f);
        }

        if (YouNowFood)
        {
            float t = (Time.time - startTime) / duration;
            sprite.color = new Color(0.5f, 0.5f, 0.5f, Mathf.SmoothStep(maximum, minimum, t));
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ScoreScript.Score = 0;
            Application.LoadLevel(0);
        }
    }
}
