﻿using UnityEngine;
using System.Collections;

public class EnemyBehaviour : MonoBehaviour {
    public float minimum = 0f;
    public float maximum = 1f;
    public float duration = 5f;
    private float startTime;
    private float eatMass;
    private float size;
    private bool eaten = false;
    private bool fadeOut = false;

    private float tChange = 0; // force new direction in the first Update
    private float randomX;
    private float randomY;
    private float moveSpeed;

    public SpriteRenderer sprite;
	// Use this for initialization
	void Start () {
        size = transform.localScale.x;
        moveSpeed = 1/(2*size);
        eatMass = size / 5;
    }
	
	// Update is called once per frame
	void Update () {

        if (Time.time >= tChange)
        {
            randomX = Random.Range(-2.0f, 2.0f); // with float parameters, a random float
            randomY = Random.Range(-2.0f, 2.0f); //  between -2.0 and 2.0 is returned
                                                 // set a random interval between 0.5 and 1.5
            tChange = Time.time + Random.Range(0.5f, 1.5f);
        }
        transform.Translate(new Vector3(randomX, randomY, 0) * moveSpeed * Time.deltaTime);


        if (fadeOut)
        {
            float t = (Time.time - startTime) / duration;
            sprite.color = new Color(1f,1f,1f, Mathf.SmoothStep(maximum, minimum, t));
        }

        if (ScoreScript.Score == 19)
        {
            Win();
        }
    }

    void food ()
    {
        Control.size += eatMass;
        ScoreScript.Score++;
    }

    void OnTriggerEnter2D (Collider2D coll)
    {
        if (coll.tag == "Player") {

            randomX *= -1;
            randomY *= -1;

            if ((Control.size > size) && (!eaten))
            {
                startTime = Time.time;
                fadeOut = true;
                StartCoroutine(nowFood(1f));
                food();
                eaten = true;
            }

            if ((Control.size < size) && (!eaten))
            {
                Control.startTime = Time.time;
                Control.YouNowFood = true;
                StartCoroutine(GOTimer(2));
            }
        }

        if (coll.tag == "Enemy")
        {
            randomX *= -1;
            randomY *= -1;
        }

        if (coll.tag == "HWall")
        {
            randomX *= -1;
        }

        if (coll.tag == "VWall")
        {
            randomY *= -1;
        }


    }

    void Win()
    {
        Application.LoadLevel(2);
    }
    void GameOver()
    {
        Application.LoadLevel(1);
    }

    IEnumerator GOTimer (float GOTime)
    {
        yield return new WaitForSeconds(GOTime);
        GameOver();
    }

    IEnumerator nowFood(float disgustTime)
    {
        yield return new WaitForSeconds(disgustTime);
        gameObject.SetActive(false);
    }
}
